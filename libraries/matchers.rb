if defined?(ChefSpec)
  def install_jar_file(name)
    ChefSpec::Matchers::ResourceMatcher.new(:jar_file, :install, name)
  end

  def install_jar_file_from_snapshot(name)
    ChefSpec::Matchers::ResourceMatcher.new(:jar_file_from_snapshot, :install, name)
  end
end
