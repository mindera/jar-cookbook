# War Cookbook

Library cookbook to deploy jar files.

## Requirements

None.

## Platforms

 * Centos 6+
 * Amazon Linux

## Attributes

No default attributes.

## Recipes

### default

The default recipe is a no-op.

## Providers

Providers are available to application cookbooks - you should get familiar with the library/application cookbook pattern.

### jar_file

This resource will retrieve a jar file from a remote repository and deploy it into a filesystem path.

Attributes:

- `name` - name of the jar file to deploy - required.
- `url` - where to retrieve the jar file - required.
- `path` -  where to deploy the jar file - required.
- `owner` - system user to own the jar file - optional, defaults to `tomcat`.
- `group` - system group to own the jar file - optional, defaults to `tomcat`.

Usage:

```ruby
jar_file 'foo.jar' do
  url 'http://something.foobar/foo.jar'
  path '/var/lib/tomcat/webapps'
end
```

This will retrieve the jar available at `http://something.foobar/foo.jar` and deploy it to `/var/lib/tomcat/webapps/foo.jar`.

### jar_file_from_snapshot

This resource will fetch the latest version of a snapshot artifact from a remote repository based on the maven metadata xml file and deploy it into a filesystem path.

Attributes:

- `name` - name of the jar file to deploy - required.
- `artifact` - name of the jar artifact - required.
- `classifier` - name of the classifier used at build time - optional.
- `metadata_url` - where to retrieve the metadata xml file - required.
- `path` -  where to deploy the jar file - required.
- `owner` - system user to own the jar file - optional, defaults to `tomcat`.
- `group` - system group to own the jar file - optional, defaults to `tomcat`.

Usage:

```ruby
jar_file_from_snapshot 'foo.jar' do
  artifact 'foo'
  metadata_url 'http://something.foobar/foo/some.version-SNAPSHOT/maven-metadata.xml'
  path '/var/lib/tomcat/webapps'
end
```

This will fetch the metadata xml file available at `http://something.foobar/foo/some.version-SNAPSHOT/maven-metadata.xml`, parse it to fetch the latest snapshot available and fetch the correspondent jar deploy it to `/var/lib/tomcat/webapps/foo.jar`.

It assumes the maven metadata xml file has a structure such as:

 - using xpath: `/metadata/versioning/snapshotVersions/snapshotVersion[1]/value/text()`

## Development / Contributing

### Tests

TODO
